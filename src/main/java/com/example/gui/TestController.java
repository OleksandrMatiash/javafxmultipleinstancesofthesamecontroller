package com.example.gui;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TestController {

    @FXML
    private Pane paneToReplaceByProgress1;
    @FXML
    private Pane paneToReplaceByProgress2;

    private ProgressController p1Controller;
    private ProgressController p2Controller;

    @FXML
    private void initialize() {
        p1Controller = injectNewProgressAndGetController(paneToReplaceByProgress1);
        p2Controller = injectNewProgressAndGetController(paneToReplaceByProgress2);

        p1Controller.bind(new SimpleDoubleProperty(0.5));
        p2Controller.bind(new SimpleDoubleProperty(0.25));
    }

    private ProgressController injectNewProgressAndGetController(Pane removableProgressContainer1) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("gui/ProgressPanel.fxml"));
            HBox box = loader.load();

            ObservableList<Node> children = ((Pane) removableProgressContainer1.getParent()).getChildren();
            children.set(children.indexOf(removableProgressContainer1), box);
            return loader.getController();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
