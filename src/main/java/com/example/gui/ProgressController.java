package com.example.gui;

import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import org.springframework.stereotype.Service;

@Service
public class ProgressController {

    @FXML
    private ProgressBar progressBar;

    @FXML
    private void initialize() {
        progressBar.progressProperty().unbind();
        progressBar.progressProperty().setValue(0);
    }

    public void bind(DoubleProperty progressProperty) {
        progressBar.progressProperty().bind(progressProperty);
    }
}
